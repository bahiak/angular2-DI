import { Component, OnInit } from '@angular/core';

import { IProduct } from './product.model';

import { ProductService } from './product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html'
})

export class ProductsComponent implements OnInit {

  pageTitle: string = "List of products";
  products: IProduct[];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this.products = this._productService.getProducts();
  }

}
