export interface IProduct {
    productId: number;
    productName: string;
    productCode: string;
    productAvailable: number;
    productPrice: number;
}
