import { Injectable } from '@angular/core';

import { IProduct } from './product.model';

@Injectable()
export class ProductService {

  constructor() { }

  getProducts(): IProduct[]{
      return [
        {
          productId: 1,
          productName: "Item1",
          productCode: "PRD01",
          productAvailable: 200,
          productPrice: 50,
        },
        {
          productId: 2,
          productName: "Item2",
          productCode: "PRD02",
          productAvailable: 100,
          productPrice: 25,
        }
      ];
  }
}
